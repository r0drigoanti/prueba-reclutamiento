import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Mining';

  mostrar: string = "";

  showParrafos(){
    this.mostrar = "parrafos";
  }
  showNumeros(){
    this.mostrar = "numeros";
  }
}
