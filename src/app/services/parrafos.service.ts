import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ParrafosService {

  constructor(private http: HttpClient) {

  }

  readonly api_url = 'http://168.232.165.184/prueba/dict';

  getParrafos() {
    return this.http.get(this.api_url);
  }

}
