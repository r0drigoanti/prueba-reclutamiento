import { Component, OnInit } from '@angular/core';
import { NumbersService } from '../../services/numbers.service';
import { IRemoteResponse } from '../../models/number'


@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styleUrls: ['./numbers.component.css']
})

export class NumbersComponent implements OnInit {

  constructor(private NumbersService: NumbersService) { 

  }

  Numeros : IRemoteResponse;
  numeros_ev: Array<{indice:number; numero:number; ocurrencia: number; posicion:number}> = [];
  numeros_ordenados: Array<{numero:number}> = [];
  estado:any;


  ngOnInit() {
    this.getNumbers();
  }
  
  getNumbers(){
    
    this.NumbersService.getNumbers()  
    .subscribe(data => {
      var posicion = 0;
      if(data["success"]==true)
      {
        this.estado="true";
        this.Numeros = data["data"];
        let largo = Object.keys(this.Numeros).length;
        console.log("largo: "+largo);
  
        let indice = 0;
  
          for (var i=0; i< largo; i++)
          {
            let ocurrencia = 0;
            let numero_evaluar = this.Numeros[i];
            let already_exists = 0;
              console.log("Evaluando número: "+numero_evaluar);
              for(var h = 0; h < this.numeros_ev.length; h++ )
              {
                if(numero_evaluar === this.numeros_ev[h]["numero"])
                {
                  already_exists = 1;
                  console.log("Numero ya existe en el arreglo");
                }
              }
              
              if(already_exists==0)
              {
                for(var j = 0; j< largo; j++)
                {
                  if(numero_evaluar === this.Numeros[j])
                  {               
                    ocurrencia ++;
                  }
                }
                indice++;
                if(indice == 1)
                {
                  posicion = 1;
                }
                else 
                {
                  posicion = 0;
                }
                this.numeros_ev.push({"indice":indice,"numero":numero_evaluar,"ocurrencia": ocurrencia, "posicion": posicion});
               
              } 
            
            //console.log("numero: "+numero_evaluar+" concurrencia: "+concurrencias);
          }
          var largo_evaluados = this.numeros_ev.length;
          this.numeros_ev[largo_evaluados-1].posicion = 9; // marcamos en nuestro arreglo la última posición
          console.log(this.numeros_ev);
  
          for(var y = 0; y < largo_evaluados; y++){
            this.numeros_ordenados.push({"numero":this.numeros_ev[y]["numero"]});          
          }
          this.ordenarNumeros();
      }
      else
      {
        this.estado="false";
      }
    }
      
 
   );

  }


  ordenarNumeros(){

    var largo = this.numeros_ordenados.length;
    
    for (var x = 0; x < largo; x++) {
        for (var i = 0; i < largo-x-1 ; i++) {
            if(this.numeros_ordenados[i]["numero"] > this.numeros_ordenados[i+1]["numero"]){
                var tmp = this.numeros_ordenados[i+1]["numero"];
                this.numeros_ordenados[i+1]["numero"] = this.numeros_ordenados[i]["numero"];
                this.numeros_ordenados[i]["numero"] = tmp;
            }
        }
    }
    console.log(this.numeros_ordenados);
    
  }


}
